package com.example.issuetrackerapp.controller;

import com.example.issuetrackerapp.Utility.Constants;
import com.example.issuetrackerapp.Utility.Util;
import com.example.issuetrackerapp.entity.Developer;
import com.example.issuetrackerapp.model.Response;
import com.example.issuetrackerapp.service.DeveloperService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = {"Developer"}, description = "Here  you can ADD and Remove Developer")
@RequestMapping("/developer")
public class DevelopersController {


    private final DeveloperService developerService;

    @Autowired
    public DevelopersController(DeveloperService developerService) {
        this.developerService = developerService;
    }

    @PostMapping("/AddDeveloper")
    public ResponseEntity<?> save(@RequestBody Developer developer) {
        Response response;
        try {
            response = developerService.save(developer);
            return Util.createResponseEntity(response);
        } catch (Exception ex) {
            ex.printStackTrace();
            response = Util.createResponse(Constants.FAIL, "Failed to save Developer Data : " + ex.getMessage(), null);
            return Util.createResponseEntity(response);
        }
    }

    @DeleteMapping("/RemoveDeveloper/{id}")
    public ResponseEntity<?> save(@PathVariable int id) {
        Response response;
        try {
            response = developerService.delete(id);
            return Util.createResponseEntity(response);
        } catch (Exception e) {
            e.printStackTrace();
            response = Util.createResponse(Constants.FAIL, "Failed to delete Developer data" + e.getMessage(), null);
            return Util.createResponseEntity(response);
        }
    }

}
