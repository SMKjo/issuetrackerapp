package com.example.issuetrackerapp.controller;

import com.example.issuetrackerapp.Utility.Constants;
import com.example.issuetrackerapp.Utility.Util;
import com.example.issuetrackerapp.entity.Story;
import com.example.issuetrackerapp.model.Response;
import com.example.issuetrackerapp.service.StoryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = {"Story"}, description = "Here  you can ADD and Remove Story")
@RequestMapping("/story")
public class StoryController {


    private final StoryService storyService;

    @Autowired
    public StoryController(StoryService storyService) {
        this.storyService = storyService;
    }


    @PostMapping("/addStory")
    public ResponseEntity<?> save(@RequestBody Story story) {
        Response response;
        try {
            response = storyService.save(story);
            return Util.createResponseEntity(response);
        } catch (Exception ex) {
            ex.printStackTrace();
            response = Util.createResponse(Constants.FAIL, "Failed to save Story Data : " + ex.getMessage(), null);
            return Util.createResponseEntity(response);
        }
    }

    @DeleteMapping("/RemoveStory/{id}")
    public ResponseEntity<?> save(@PathVariable int id) {
        Response response;
        try {
            response = storyService.delete(id);
            return Util.createResponseEntity(response);
        } catch (Exception e) {
            e.printStackTrace();
            response = Util.createResponse(Constants.FAIL, "Failed to delete Story data" + e.getMessage(), null);
            return Util.createResponseEntity(response);
        }
    }

}
