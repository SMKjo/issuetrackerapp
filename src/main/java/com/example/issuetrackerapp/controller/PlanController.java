package com.example.issuetrackerapp.controller;

import com.example.issuetrackerapp.Utility.Constants;
import com.example.issuetrackerapp.Utility.Util;
import com.example.issuetrackerapp.model.Response;
import com.example.issuetrackerapp.service.PlanService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = {"Plan"}, description = "Here  you can get Number of Weeks Required and Create Plan after adding stories and developers")
@RequestMapping("/plan")
public class PlanController {


    private final PlanService planService;

    @Autowired
    public PlanController(PlanService planService) {
        this.planService = planService;
    }


    @GetMapping("/getNoOfWeekRequired")
    public ResponseEntity<?> getPlan() {
        Response response;
        try {
            Double noOfWeekReq = planService.getNoOfWeekRequired();
            response = Util.createResponse(Constants.SUCCESS, "No of week Required is ", noOfWeekReq);
            return Util.createResponseEntity(response);
        } catch (Exception ex) {
            ex.printStackTrace();
            response = Util.createResponse(Constants.FAIL, "Failed to get Plan : " + ex.getMessage(), null);
            return Util.createResponseEntity(response);
        }
    }

    @GetMapping("/createPlan")
    public ResponseEntity<?> createPlan() {
        Response response;
        try {
            response = planService.createPlan();
            return Util.createResponseEntity(response);
        } catch (Exception ex) {
            ex.printStackTrace();
            response = Util.createResponse(Constants.FAIL, "Failed to get Plan : " + ex.getMessage(), null);
            return Util.createResponseEntity(response);
        }
    }

}
