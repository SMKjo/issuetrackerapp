package com.example.issuetrackerapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "plan")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Plan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "plan_id")
    private long planId;

    @Column(name = "week_no")
    private int weekNumber;

    @Column(name = "title")
    private String title;

    @Column(name = "estimated_points")
    private int points;

    @Column(name = "point_remaining")
    private int pointRemaining;

    @Column(name = "Status")
    private String Status;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getPointRemaining() {
        return pointRemaining;
    }

    public void setPointRemaining(int pointRemaining) {
        this.pointRemaining = pointRemaining;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}




