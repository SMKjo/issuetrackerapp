package com.example.issuetrackerapp.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "story")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Story {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "issue_id")
    private int issueId;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "create_date")
    private Date createDate;

    @ManyToMany
    @JsonIgnore
    private List<Developer> developers;

    @Column(name = "estimated_point_value")
    private int estimatedPointValue;

    @Column(name = "status")
    private String status;


    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(Developer developer) {
        this.developers.add(developer);
    }

    public int getEstimatedPointValue() {
        return estimatedPointValue;
    }

    public void setEstimatedPointValue(int estimatedPointValue) {
        this.estimatedPointValue = estimatedPointValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}




