package com.example.issuetrackerapp.Utility;

import com.example.issuetrackerapp.model.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class Util {


    public static Response createResponse(String status, String msg, Object obj) {
        Response response = new Response();
        response.setStatus(status);
        response.setMessage(msg);
        response.setObject(obj);
        return response;
    }

    public static ResponseEntity createResponseEntity(Response response) {
        if (response.getStatus().equals(Constants.SUCCESS)) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
