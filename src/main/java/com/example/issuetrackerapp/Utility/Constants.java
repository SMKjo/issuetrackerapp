/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.issuetrackerapp.Utility;

public interface Constants {

    public static final String NEW = "New";
    public static final String SUCCESS = "Success";
    public static final String FAIL = "Fail";
    public static final String ERROR = "Error";
    public static final String COMPLETED = "Completed";
    public static final String IN_PROGRESS = "In-Progress";


}
