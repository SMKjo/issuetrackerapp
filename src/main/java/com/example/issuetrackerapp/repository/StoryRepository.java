/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.issuetrackerapp.repository;

import com.example.issuetrackerapp.entity.Story;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface StoryRepository extends JpaRepository<Story, Integer> {

    public Story findStoriesByIssueId(@Param("issue_id") long issueId);
}
