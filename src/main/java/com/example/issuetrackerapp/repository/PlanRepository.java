/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.issuetrackerapp.repository;

import com.example.issuetrackerapp.entity.Plan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PlanRepository extends JpaRepository<Plan, Integer> {

    @Query(value = "SELECT * FROM plan WHERE title =:title", nativeQuery = true)
    List<Plan> getAllByStory(@Param("title") String title);
}
