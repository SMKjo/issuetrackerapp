/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.issuetrackerapp.repository;

import com.example.issuetrackerapp.entity.Developer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DevelopersRepository extends JpaRepository<Developer, Integer> {
}
