package com.example.issuetrackerapp.service;

import com.example.issuetrackerapp.Utility.Constants;
import com.example.issuetrackerapp.Utility.Util;
import com.example.issuetrackerapp.entity.Story;
import com.example.issuetrackerapp.model.Response;
import com.example.issuetrackerapp.repository.StoryRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class StoryService {

    private static final Logger logger = Logger.getLogger(StoryService.class.getName());
    private final StoryRepository storyRepository;

    public StoryService(StoryRepository storyRepository) {
        this.storyRepository = storyRepository;
    }

    public Response save(Story story) {
        Response response;
        story = storyRepository.save(story);

        if (story != null) {
            response = Util.createResponse(Constants.SUCCESS, "Story data saved successfully", story);
            logger.log(Level.INFO, "Story saved successfully");
        } else {
            response = Util.createResponse(Constants.FAIL, "Failed to save Story data", null);
            logger.log(Level.WARNING, "Failed to save Story data");
        }

        return response;
    }

    public List<Story> getAllStories() {
        List<Story> stories = storyRepository.findAll();
        if (stories.size() != 0) {
            return stories;
        } else {
            return null;
        }
    }

    public Response delete(int id) {
        Response response;
        try {
            storyRepository.deleteById(id);
            response = Util.createResponse(Constants.SUCCESS, "Story deleted successfully", null);
            logger.log(Level.INFO, "Story deleted successfully");
        } catch (Exception e) {
            response = Util.createResponse(Constants.FAIL, "Failed to delete Story data", e.getMessage());
            logger.log(Level.WARNING, "Failed to delete Story data");
        }
        return response;
    }


}
