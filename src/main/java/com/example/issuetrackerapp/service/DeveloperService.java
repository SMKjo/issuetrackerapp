package com.example.issuetrackerapp.service;

import com.example.issuetrackerapp.Utility.Constants;
import com.example.issuetrackerapp.Utility.Util;
import com.example.issuetrackerapp.entity.Developer;
import com.example.issuetrackerapp.model.Response;
import com.example.issuetrackerapp.repository.DevelopersRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class DeveloperService {

    private static final Logger logger = Logger.getLogger(DeveloperService.class.getName());
    private final DevelopersRepository developersRepository;

    public DeveloperService(DevelopersRepository developersRepository) {
        this.developersRepository = developersRepository;
    }

    public Response save(Developer developer) {
        Response response;
        developer = developersRepository.save(developer);

        if (developer != null) {
            response = Util.createResponse(Constants.SUCCESS, "Developer data saved successfully", developer);
            logger.log(Level.INFO, "Developer saved successfully");
        } else {
            response = Util.createResponse(Constants.FAIL, "Failed to save Developer data", null);
            logger.log(Level.WARNING, "Failed to save Developer data");
        }

        return response;
    }

    public Response delete(int id) {
        Response response;
        try {
            developersRepository.deleteById(id);
            response = Util.createResponse(Constants.SUCCESS, "Developer data deleted successfully", null);
            logger.log(Level.INFO, "Developer deleted successfully");
        } catch (Exception e) {
            response = Util.createResponse(Constants.FAIL, "Failed to delete Developer data", e.getMessage());
            logger.log(Level.WARNING, "Failed to delete Developer data");
        }
        return response;
    }

    public List<Developer> getAllDevs() {

        List<Developer> developerList = developersRepository.findAll();

        if (developerList.size() != 0) {
            return developerList;
        } else {
            return null;
        }
    }
}
