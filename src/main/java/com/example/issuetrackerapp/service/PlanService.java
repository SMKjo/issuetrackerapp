package com.example.issuetrackerapp.service;

import com.example.issuetrackerapp.Utility.Constants;
import com.example.issuetrackerapp.Utility.Util;
import com.example.issuetrackerapp.entity.Developer;
import com.example.issuetrackerapp.entity.Plan;
import com.example.issuetrackerapp.entity.Story;
import com.example.issuetrackerapp.model.Response;
import com.example.issuetrackerapp.repository.PlanRepository;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.temporal.IsoFields;
import java.util.ArrayList;
import java.util.List;

@Component
public class PlanService {

    private final DeveloperService developerService;
    private final StoryService storyService;
    private final PlanRepository planRepository;

    public PlanService(DeveloperService developerService, StoryService storyService, PlanRepository planRepository) {
        this.developerService = developerService;
        this.storyService = storyService;
        this.planRepository = planRepository;
    }

    public double getNoOfWeekRequired() {
        List<Developer> developerList = developerService.getAllDevs();
        List<Story> storyList = storyService.getAllStories();
        double perWeekEstimatedPoint = developerList.size() * 10;
        double totalEstimatedPoints = storyList.stream().mapToDouble(Story::getEstimatedPointValue).sum();
        return Math.ceil(totalEstimatedPoints / perWeekEstimatedPoint);
    }

    public Boolean checkPlanOfStoryExists(Story story) {
        List<Plan> existingPlan = planRepository.getAllByStory(story.getTitle());
        if (existingPlan.isEmpty()) {
            return false;
        } else {
            return existingPlan.stream().anyMatch(plan -> plan.getTitle().equals(story.getTitle()));
        }
    }

    public Response createPlan() {
        Response response;
        List<Developer> developerList = developerService.getAllDevs();
        List<Story> storyList = storyService.getAllStories();
        List<Plan> planList = new ArrayList<>();

        if (storyList.isEmpty()) {
            return null;
        } else if (developerList.size() > 1 || storyList.size() > 1) {
            for (Story story : storyList) {
                if (!checkPlanOfStoryExists(story)) {

                    int week = calculatePlanCount(story.getEstimatedPointValue(), developerList);
                    int weekNo = story.getCreateDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().get(IsoFields.WEEK_OF_WEEK_BASED_YEAR);
                    int points = story.getEstimatedPointValue() - (developerList.size() * 10);
                    for (Developer developer : developerList) {
                        story.setDevelopers(developer);
                        if (points < 0) {
                            story.setStatus(Constants.COMPLETED);
                        } else {
                            story.setStatus(Constants.IN_PROGRESS);
                        }
                        storyService.save(story);
                    }
                    for (int i = 1; i <= week; i++) {
                        Plan plan = new Plan();
                        plan.setWeekNumber(weekNo);
                        plan.setTitle(story.getTitle());
                        plan.setPoints(story.getEstimatedPointValue());
                        if (points < 0) {
                            plan.setPointRemaining(0);
                        } else {
                            plan.setPointRemaining(points);
                        }
                        if (plan.getPointRemaining() == 0 || plan.getPointRemaining() < 0) {
                            plan.setStatus(Constants.COMPLETED);
                        } else {
                            plan.setStatus(Constants.IN_PROGRESS);
                        }
                        planList.add(plan);
                        points -= (developerList.size() * 10);
                        weekNo += 1;
                    }
                }
            }
            planRepository.saveAll(planList);
        }
        response = Util.createResponse(Constants.SUCCESS, "Plan created successfully", planRepository.findAll());
        return response;
    }

    public int calculatePlanCount(int estimatedPoint, List<Developer> developers) {
        int value = estimatedPoint / 10;
        int planCount;
        if (value % developers.size() == 0) {
            planCount = value / developers.size();
        } else {
            planCount = value / developers.size();
            planCount = planCount + 1;
        }
        return planCount;
    }

}
