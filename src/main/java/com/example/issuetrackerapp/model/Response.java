/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.issuetrackerapp.model;

public class Response {

    private String status;
    private Object object;
    private String message;

    public Response(String status, Object object, String message) {
        this.status = status;
        this.object = object;
        this.message = message;
    }

    public Response() {

    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getObject() {
        return this.object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
